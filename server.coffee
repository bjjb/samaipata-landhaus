express = require('express')

app = express()

app.use require('morgan')('tiny')
app.use express.static 'public'

app.get '/', (req, res) ->
  res.render('index')

app.listen (process.env.PORT or 3000), ->
  {address, port} = @address()
  {name, version} = require './package'
  console.log "#{name} v#{version} listening on #{address}:#{port}"
