Samaipata Landhaus
==================

This is the website for Samaipata Landhaus. It's in 4 languages, and the
actual website lives in the public/ directory.

Prerequisites
-------------

The site is static, so needs nothing to run other than a standard HTTP server.
However, to build the site, you should have [Git][], and [Node.js][] (with
[NPM][], and [Grunt][]. You'll also need [Ruby][] to run [SASS][].

Editing and Building
--------------------

To change the website, first check it out:

    git clone git://bitbucket.org/bjjb/samaipata-landhaus.git

Then, in the directory, make sure you have the dependencies (Jade, SASS,
CoffeeScript, Express and morgan for running the local server, and some Grunt
plugins). Install them with

    npm install

Now you can make your changes, and to build the site, just run

    grunt

Structure
---------

The HTML files are compiled from Jade templates in the jade/ directory. The
internationalized content lives in YAML files in locale/ (it's here that you
change the text). 

The style is built from SASS in the sass/ directory, into public/css/.

Javascript is built from CoffeeScript in the coffee/ directory, into
public/js/.

All of these are built by the grunt command.

Photos and whatnot are already in public/images/.

Deploying
---------

You can deploy to Heroku (if you have permission) with

    git push heroku master

It will then be available on samaipata-landhaus.herokuapp.com

The production website it hosted by matpec. To deploy there, run

    grunt publish

This builds and FTPs up the public/ directory.

[Git]: http://git-scm.com
[Node.js]: http://nodejs.org
[NPM]: http://npmjs.com
[Grunt]: http://gruntjs.com
[SASS]: http://sass-lang.com
[Ruby]: http://ruby-lang.org
