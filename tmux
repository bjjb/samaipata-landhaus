#!/usr/bin/env sh
s="samaipata-landhaus"
tmux has-session -t $s
if [ $? != 0 ]
then
  tmux new-session   -s $s          -n editor -d
  tmux send-keys     -t $s          'vim .' C-m
  tmux new-window    -t $s          -n build
  tmux split-window  -t $s:build
  tmux select-layout -t $s:build   even-vertical
  tmux send-keys     -t $s:build.1 'npm start' C-m
fi
tmux attach-session -t $s
