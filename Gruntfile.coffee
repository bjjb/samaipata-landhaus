marked = require 'marked'

module.exports = (grunt) ->
  data = (locale) ->
    obj = grunt.file.readYAML("locale/#{locale}.yml")[locale]
    obj.locale = locale
    obj.config = grunt.file.readYAML('config.yml')
    obj.marked = marked
    obj

  grunt.initConfig
    pkg: grunt.file.readJSON('package.json')

    jade:
      index:
        files:
          "public/index.html": "jade/index.jade"
        options:
          data: data('en')
      en:
        files:
          "public/en.html": "jade/index.jade"
        options:
          data: data('en')
      de:
        files:
          "public/de.html": "jade/index.jade"
        options:
          data: data('de')
      es:
        files:
          "public/es.html": "jade/index.jade"
        options:
          data: data('es')
      nl:
        files:
          "public/nl.html": "jade/index.jade"
        options:
          data: data('nl')
    sass:
      compile:
        files:
          "public/css/main.css": "sass/main.scss"
    coffee:
      compile:
        files:
          "public/js/main.js": "coffee/main.coffee"

    'ftp-deploy':
      publish:
        src: 'public'
        dest: 'public_html'
        auth:
          host: 'www.samaipata-landhaus.com'
    # If deploy doesn't work, try lftp, with
    # mirror -Rv public/ public_html/
    # You might get certificat errors unless you do
    # mkdir -p ~/.lftp && echo 'set ssl:check-hostname false;' >> ~/.lftp/rc

  grunt.loadNpmTasks('grunt-contrib-jade')
  grunt.loadNpmTasks('grunt-contrib-coffee')
  grunt.loadNpmTasks('grunt-contrib-sass')
  grunt.loadNpmTasks('grunt-ftp-deploy')

  grunt.registerTask 'default', ["jade:index", "jade:en", "jade:es", "jade:de", "jade:nl", "sass:compile", "coffee:compile"]
  grunt.registerTask 'publish', ['default', 'ftp-deploy:publish']
