(function() {
  var $body, $header, $window, scrollTop, sendEmail;

  skel.breakpoints({
    xlarge: '(max-width: 1800px)',
    large: '(max-width: 1280px)',
    medium: '(max-width: 980px)',
    small: '(max-width: 736px)',
    xsmall: '(max-width: 480px)'
  });

  $window = $(window);

  $body = $('body');

  $header = $('#header');

  $body.addClass('is-loading');

  $window.on('load', function() {
    return $body.removeClass('is-loading');
  });

  if (skel.vars.mobile) {
    $body.addClass('is-touch');
  }

  scrollTop = function() {
    return $window.scrollTop($window.scrollTop() + 1);
  };

  window.setTimeout(scrollTop, 0);

  $('form').placeholder();

  skel.on('+medium -medium', function() {
    return $.prioritize('.important\\28 medium\\29', skel.breakpoint('medium').active);
  });

  $window.on('load', function() {
    var ref, ref1;
    $('#houses,#rooms').poptrox({
      caption: function($a) {
        return $a.next('p').text();
      },
      overlayColor: '#2c2c2c',
      overlayOpacity: 0.85,
      popupCloserText: '',
      popupLoaderText: '',
      selector: 'a.image',
      usePopupDefaultStyling: false,
      usePopupEasyClose: false,
      usePopupNav: true,
      windowMargin: (ref = skel.breakpoint('small').active) != null ? ref : {
        0: 50
      }
    });
    return $('#samaipata,#gallery').poptrox({
      overlayColor: '#2c2c2c',
      overlayOpacity: 0.85,
      popupCloserText: '',
      popupLoaderText: '',
      selector: 'a.image',
      usePopupCaption: true,
      usePopupDefaultStyling: false,
      usePopupEasyClose: false,
      usePopupNav: true,
      windowMargin: (ref1 = skel.breakpoint('small').active) != null ? ref1 : {
        0: 50
      }
    });
  });

  sendEmail = function(event) {
    var _subject, always, data, dataType, done, email, fail, message, method, name, url;
    event.preventDefault();
    $(this).removeClass('error', 'success').addClass('sending');
    $('#contact [type="submit"]').attr('disabled', true);
    url = $(this).attr('action');
    method = $(this).attr('method');
    name = this.name.value;
    email = this.email.value;
    _subject = this._subject.value;
    message = this.message.value;
    data = {
      name: name,
      email: email,
      _subject: _subject,
      message: message
    };
    dataType = 'json';
    done = sendEmail.done, fail = sendEmail.fail, always = sendEmail.always;
    console.log("Sending!!", url, {
      method: method,
      data: data,
      dataType: dataType
    });
    return $.ajax(url, {
      method: method,
      data: data,
      dataType: dataType
    }).done(done).fail(fail).always(always);
  };

  sendEmail.done = function() {
    return $('#contact form').addClass('success');
  };

  sendEmail.fail = function() {
    $('#contact form').addClass('error');
    return $('#contact [type="submit"]').removeAttr('disabled');
  };

  sendEmail.always = function() {
    return $('#contact form').removeClass('sending');
  };

  $window.on('load', function() {
    return $('#contact form').on('submit', sendEmail);
  });

}).call(this);
