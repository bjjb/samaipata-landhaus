skel.breakpoints
  xlarge: '(max-width: 1800px)'
  large: '(max-width: 1280px)'
  medium: '(max-width: 980px)'
  small: '(max-width: 736px)'
  xsmall: '(max-width: 480px)'

$window = $(window)
$body = $('body')
$header = $('#header')

# Disable animations/transitions until the page has loaded.
$body.addClass('is-loading')
$window.on 'load', -> $body.removeClass('is-loading')

# Turn on touch mode.
$body.addClass('is-touch') if skel.vars.mobile

# Height fix (mostly for iOS).
scrollTop = -> $window.scrollTop($window.scrollTop() + 1)
window.setTimeout scrollTop, 0

# Fix: Placeholder polyfill.
$('form').placeholder()

# Prioritize "important" elements on medium.
skel.on '+medium -medium', ->
  $.prioritize('.important\\28 medium\\29', skel.breakpoint('medium').active)

# Lightbox gallery.
$window.on 'load', ->
  $('#houses,#rooms').poptrox
    caption: ($a) -> $a.next('p').text()
    overlayColor: '#2c2c2c'
    overlayOpacity: 0.85
    popupCloserText: ''
    popupLoaderText: ''
    selector: 'a.image'
    #usePopupCaption: true
    usePopupDefaultStyling: false
    usePopupEasyClose: false
    usePopupNav: true
    windowMargin: skel.breakpoint('small').active ? 0 : 50
  $('#samaipata,#gallery').poptrox
    overlayColor: '#2c2c2c'
    overlayOpacity: 0.85
    popupCloserText: ''
    popupLoaderText: ''
    selector: 'a.image'
    usePopupCaption: true
    usePopupDefaultStyling: false
    usePopupEasyClose: false
    usePopupNav: true
    windowMargin: skel.breakpoint('small').active ? 0 : 50

sendEmail = (event) ->
  event.preventDefault()
  $(@).removeClass('error', 'success').addClass('sending')
  $('#contact [type="submit"]').attr('disabled', true)
  url      = $(@).attr('action')
  method   = $(@).attr('method')
  name     = @name.value
  email    = @email.value
  _subject = @_subject.value
  message  = @message.value
  data     = {name, email, _subject, message}
  dataType = 'json'
  { done, fail, always } = sendEmail
  console.log "Sending!!", url, { method, data, dataType }
  $.ajax(url, {method, data, dataType}).done(done).fail(fail).always(always)

sendEmail.done = ->
  $('#contact form').addClass('success')

sendEmail.fail = ->
  $('#contact form').addClass('error')
  $('#contact [type="submit"]').removeAttr('disabled')

sendEmail.always = ->
  $('#contact form').removeClass('sending')

# Contact form
$window.on 'load', ->
  $('#contact form').on 'submit', sendEmail
